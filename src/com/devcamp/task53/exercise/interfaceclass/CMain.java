package com.devcamp.task53.exercise.interfaceclass;

import java.util.ArrayList;

public class CMain {

	public static void main(String[] args) {
		//TODO Auto-generated method stub
		CPet dog = new CDog();
		dog.animalSound();
		dog.eat();

		CPet cat = new CCat();
		cat.animalSound();
		cat.eat();

		ArrayList<CPet> petsList = new ArrayList<>();
		petsList.add(dog);
		petsList.add(cat);


		CPerson person = new CPerson("Thanh",28,petsList);
		CPerson person1 = new CPerson();

		ArrayList<CPerson> personlist = new ArrayList<CPerson>();
		personlist.add(person);
		personlist.add(person1);


		System.out.println(person);

	} 
}

