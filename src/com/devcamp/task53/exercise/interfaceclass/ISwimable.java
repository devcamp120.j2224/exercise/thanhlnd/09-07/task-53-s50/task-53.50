package com.devcamp.task53.exercise.interfaceclass;

public interface ISwimable {
	void swim();
}
