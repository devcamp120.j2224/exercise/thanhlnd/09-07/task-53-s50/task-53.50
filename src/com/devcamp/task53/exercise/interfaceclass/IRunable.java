package com.devcamp.task53.exercise.interfaceclass;

public interface IRunable extends IBarkable {
	void run() ;
	void running() ;
}
