package com.devcamp.task53.exercise.interfaceclass;

public interface ISumable {
	String  getSum();
}
