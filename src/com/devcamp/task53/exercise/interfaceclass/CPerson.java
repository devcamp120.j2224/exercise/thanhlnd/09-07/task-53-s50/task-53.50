package com.devcamp.task53.exercise.interfaceclass;
import java.util.ArrayList;
public class CPerson extends CAnimal {
	/**
	 * 
	 */
	public CPerson() {
		System.out.println("person ko co tham so");
	}
	/**
	 * khởi tạo Cperson với đủ các tham số
	 * @param age
	 * @param name
	 * @param pets
	 */
	public CPerson( String name, int age, ArrayList<CPet> pets) {
		super();
		this.age = age;
		this.name = name;
		this.pets = pets;
	}

	private int age;
	private String name;
	private ArrayList<CPet> pets;
	@Override
	public void animalSound() {
		System.out.println("person speaking.");

	}

	@Override
	public String toString() {
		return "CPerson {\"name\":" + this.name + ", age=" + age +  
		", pets=" + this.pets + "}";

		// String strPerson = "{";
		// strPerson += "Name : " + this.name + ",";
		// strPerson += "Age : " + this.age + ",";
		// strPerson += "Pets : [";
		// for(String pet : this.pets) {
		// 	strPerson += pet + ",";
		
		// strPerson += "]}";
		// }
		// return strPerson;
	}
}
