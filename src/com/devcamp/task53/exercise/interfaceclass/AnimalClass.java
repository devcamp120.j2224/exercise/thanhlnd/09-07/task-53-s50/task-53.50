package com.devcamp.task53.exercise.interfaceclass;

public enum AnimalClass {
	fish, amphibians, reptiles, mammals, birds
}
