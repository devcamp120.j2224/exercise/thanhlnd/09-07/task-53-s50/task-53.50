package com.devcamp.task53.exercise.interfaceclass;

public abstract class CAnimal {
	public AnimalClass animclass;
	
	abstract public void animalSound();
	
	public void eat() {
		System.out.println("Animal eating...");
	};
	
}
