package com.devcamp.task53.exercise.interfaceclass;

public interface Other {
 void other();
 int other(int param);
 String other(String param);
}
