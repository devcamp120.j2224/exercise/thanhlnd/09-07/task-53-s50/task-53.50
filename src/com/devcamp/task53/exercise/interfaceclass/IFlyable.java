package com.devcamp.task53.exercise.interfaceclass;

public interface IFlyable {
	void fly();
}
